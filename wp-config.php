<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~4cuF`e!S1>;_uto:drY^2>g5pooZp-+NNL!|/SS&RdWqx(M_Rk>~-.LD%U|#CAB' );
define( 'SECURE_AUTH_KEY',  '==T*5;CmS+-TE*b98~PR T=;!z&)wj55Y87@DI/r 6[&c(n~%eirT{O31^s>(;yQ' );
define( 'LOGGED_IN_KEY',    '.NZ!(XX]wzz1}(AN<Z2q!ius(8yiew2SrO<PT1gr-_^vDQ9}+K,Ur.}L,GB+5I^y' );
define( 'NONCE_KEY',        'npSk{]$P%v$8jCB;U~&]].&_YmY6gp00m%RC[^eLx?b oAp{hb3S+v^@q[5tu*,F' );
define( 'AUTH_SALT',        '?IG`43`i.O~Eb?pW^Mx>VJ@vaBorJ_E|5{b&gIheVBPO#?H0T(0Oz2bZh0xpuOjT' );
define( 'SECURE_AUTH_SALT', 'n<`1F_i^jda)9*b6IYwKp*sLeq}|Z_L]RyswJsAb6V-tL]dy,AEi_4`0vLWrdwws' );
define( 'LOGGED_IN_SALT',   '}kt-%Ed?6`=`b<IYa}wzW@bgzt]pe:$<&ARiA*?+p0a7aLR<BDf_CR4KPG=kCuk@' );
define( 'NONCE_SALT',       'd*nF{F{Zhj9B<Y@;Scx5rH6oHm|B/f-G.s#l)SufUW;<]8ZS+_+|Z/;jI:i=DW!9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
